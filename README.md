openHEVC
========


- openHEVC
----------
Initial libav version from smarter (smarter.free.fr)

- openHEVC/wrapper
----------
Initial C wrapper from HM10.0 C++ reference SW (HEVC reference sw)

- openHEVC/wrapper/HM
----------
HM10.0 reference SW

- How to compile OpenHEVC on linux from source code
----------
* git clone git://github.com/OpenHEVC/openHEVC.git
* git checkout hm10.0
* go into OpenHEVC folder source
* mkdir build
* cd build
* cmake -DCMAKE_BUILD_TYPE=RELEASE ..
* make
* make install



